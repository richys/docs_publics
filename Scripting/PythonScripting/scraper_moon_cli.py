import requests
from bs4 import BeautifulSoup
from icalendar import Calendar, Event
from datetime import datetime
import locale

# Establecemos la configuración regional a español
locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')

# URL de la página web
url = 'https://www.calendario-365.es/luna/lunar-fases.html'

# Realizamos la solicitud GET a la página
response = requests.get(url)

# Verificamos si la solicitud fue exitosa (código 200)
if response.status_code == 200:
    # Parseamos el HTML de la página con BeautifulSoup
    soup = BeautifulSoup(response.text, 'html.parser')
    
    # Encontramos la tabla por su etiqueta <table>
    tabla = soup.find('table')
    
    # Verificamos si se encontró la tabla
    if tabla:
        # Creamos un objeto de calendario iCalendar
        cal = Calendar()
        cal.add('prodid', '-//My calendar product//mxm.dk//')
        cal.add('version', '2.0')
        
        # Iteramos sobre las filas de la tabla
        for fila in tabla.find_all('tr')[1:]:  # Empezamos desde la segunda fila para omitir las cabeceras
            # Obtenemos las celdas de cada fila
            celdas = fila.find_all('td')
            if len(celdas) >= 4:  # Verificamos que haya al menos cuatro celdas (fecha, fase lunar, descripción1, descripción2)
                fecha_str = celdas[1].text.strip()  # La fecha está en la segunda columna
                summary = f'{celdas[0].text.strip()}\n{celdas[2].text.strip()}\n{celdas[3].text.strip()}'  # Resumen con datos de las columnas 1, 3 y 4
                # Convertimos la fecha a un objeto datetime
                dtstart = datetime.strptime(fecha_str, '%d %B %Y')
                
                # Creamos un evento para cada entrada en la tabla
                event = Event()
                event.add('summary', summary)
                event.add('dtstart', dtstart)
                event.add('dtend', dtstart)  # Suponiendo que el evento dura solo un día
                cal.add_component(event)
        
        # Guardamos el calendario en un archivo .ics
        with open('fases_lunares.ics', 'wb') as f:
            f.write(cal.to_ical())
            
        print('Se ha generado el archivo fases_lunares.ics')
    else:
        print('No se encontró ninguna tabla en la página.')
else:
    print('No se pudo acceder a la página:', response.status_code)