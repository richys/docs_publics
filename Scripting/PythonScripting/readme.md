# Script de scraping para obtener calendario Lunar.

> La idea surje por obtener un calendario para saber cuando hay luna, los datos son tomados de una web que nos los provee.

Paso Previos:

```sh
python3 -m venv myenv
source myenv/bin/activate
pip3 install requests beautifulsoup4 icalendar
```

Ejecución de script`scraper_moon_cli.py`

```txt
$ python3 scraper_moon_cli.py
Cuarto menguante      | 4 enero 2024       | 04:32:56 | 401.761 km
Luna nueva            | 11 enero 2024      | 12:58:05 | 370.241 km
Cuarto creciente      | 18 enero 2024      | 04:53:55 | 369.012 km
Luna llena            | 25 enero 2024      | 18:54:43 | 400.744 km
Cuarto menguante      | 3 febrero 2024     | 00:20:08 | 394.090 km
Luna nueva            | 10 febrero 2024    | 00:00:44 | 364.924 km
Cuarto creciente      | 16 febrero 2024    | 16:02:06 | 376.832 km
Luna llena            | 24 febrero 2024    | 13:31:19 | 405.085 km
Cuarto menguante      | 3 marzo 2024       | 16:25:25 | 384.506 km
Luna nueva            | 10 marzo 2024      | 10:02:45 | 363.311 km
Cuarto creciente      | 17 marzo 2024      | 05:11:50 | 386.324 km
Luna llena            | 25 marzo 2024      | 08:01:37 | 404.667 km
Cuarto menguante      | 2 abril 2024       | 05:15:50 | 375.331 km
Luna nueva            | 8 abril 2024       | 20:23:21 | 365.527 km
Cuarto creciente      | 15 abril 2024      | 21:14:29 | 395.502 km
Luna llena            | 24 abril 2024      | 01:51:09 | 399.745 km
Cuarto menguante      | 1 mayo 2024        | 13:27:52 | 368.232 km
Luna nueva            | 8 mayo 2024        | 05:24:19 | 371.129 km
Cuarto creciente      | 15 mayo 2024       | 13:49:35 | 402.368 km
Luna llena            | 23 mayo 2024       | 15:55:52 | 391.720 km
Cuarto menguante      | 30 mayo 2024       | 19:13:39 | 364.137 km
Luna nueva            | 6 junio 2024       | 14:40:02 | 379.177 km
Cuarto creciente      | 14 junio 2024      | 07:19:53 | 405.424 km
Luna llena            | 22 junio 2024      | 03:10:32 | 382.521 km
Cuarto menguante      | 28 junio 2024      | 23:55:05 | 363.464 km
Luna nueva            | 6 julio 2024       | 00:59:17 | 388.327 km
Cuarto creciente      | 14 julio 2024      | 00:49:54 | 404.025 km
Luna llena            | 21 julio 2024      | 12:19:50 | 373.946 km
Cuarto menguante      | 28 julio 2024      | 04:54:02 | 366.259 km
Luna nueva            | 4 agosto 2024      | 13:14:21 | 396.925 km
```

**EOF**