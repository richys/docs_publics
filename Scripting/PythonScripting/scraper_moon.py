import requests
from bs4 import BeautifulSoup

# URL de la página web
url = 'https://www.calendario-365.es/luna/lunar-fases.html'

# Realizamos la solicitud GET a la página
response = requests.get(url)

# Verificamos si la solicitud fue exitosa (código 200)
if response.status_code == 200:
    # Parseamos el HTML de la página con BeautifulSoup
    soup = BeautifulSoup(response.text, 'html.parser')
    
    # Encontramos la tabla por su etiqueta <table>
    tabla = soup.find('table')
    
    # Verificamos si se encontró la tabla
    if tabla:
        # Lista para almacenar las filas
        filas = []
        
        # Iteramos sobre las filas de la tabla
        for fila in tabla.find_all('tr'):
            # Obtenemos las celdas de cada fila
            celdas = fila.find_all('td')
            if celdas:
                # Agregamos los valores de las celdas a la lista de filas
                filas.append([celda.text.strip() for celda in celdas])
        
        # Imprimimos los valores en columnas
        if filas:
            # Calculamos la longitud máxima de cada columna
            column_widths = [max(len(valor) for valor in columna) for columna in zip(*filas)]
            
            # Imprimimos las filas formateadas en columnas
            for fila in filas:
                print(' | '.join('{:{width}}'.format(valor, width=width) for valor, width in zip(fila, column_widths)))
    else:
        print('No se encontró ninguna tabla en la página.')
else:
    print('No se pudo acceder a la página:', response.status_code)