#!/bin/bash

# Ver version de debian, el nombre de versión.
DEBIAN_VERSION=$(lsb_release -cs)

# Simple banner.
cat << "EOF"
         ___           _        _ _   ____                  _          
        |_ _|_ __  ___| |_ __ _| | | / ___|  ___ _ ____   _(_) ___ ___ 
         | || '_ \/ __| __/ _` | | | \___ \ / _ \ '__\ \ / / |/ __/ _ \
         | || | | \__ \ || (_| | | |  ___) |  __/ |   \ V /| | (_|  __/
        |___|_| |_|___/\__\__,_|_|_| |____/ \___|_|    \_/ |_|\___\___|
                                                                       
                                 _____          
                                |_   _|__  _ __ 
                                  | |/ _ \| '__|
                                  | | (_) | |   
                                  |_|\___/|_|   
                                                
EOF

# Instalacion de paqueteria, y creacion de repo de tor.
echo -e "Instalacion de repos de tor en debian.\n"

apt install apt-transport-https -y &> /dev/null
sleep 2s

# Creacion de repos.
cat <<EOF > /etc/apt/sources.list.d/tor.list 
deb     [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org ${DEBIAN_VERSION} main
deb-src [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org ${DEBIAN_VERSION} main
EOF

# Descarga de la gpg-key de tor.
echo -e "Descargando la gpg-key.\n"

wget -qO- https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --dearmor | tee /usr/share/keyrings/tor-archive-keyring.gpg > /dev/null

sleep 2s

# Actualización y instalacion de tor
echo -e "Actualizando los repositorios y instalando el tor.\n"

apt update &> /dev/null
apt install -y tor deb.torproject.org-keyring &> /dev/null

sleep 2s
exit 0
