#!/bin/bash

#
# Authors RICHYS and DARKPHTON - Este script hace una copia de Seguridad de nuestro sitio wordpress.
# Date: vie jun 19 18:33:50 CEST 2020
#

echo " "
cat << "EOF"
____             _                
| __ )  __ _  ___| | ___   _ _ __  
|  _ \ / _` |/ __| |/ / | | | '_ \ 
| |_) | (_| | (__|   <| |_| | |_) |
|____/ \__,_|\___|_|\_\\__,_| .__/ 
                            |_|    
__        __            _                         
\ \      / /__  _ __ __| |_ __  _ __ ___  ___ ___ 
 \ \ /\ / / _ \| '__/ _` | '_ \| '__/ _ \/ __/ __|
  \ V  V / (_) | | | (_| | |_) | | |  __/\__ \__ \
   \_/\_/ \___/|_|  \__,_| .__/|_|  \___||___/___/
                         |_|                      

EOF
echo " "
sleep 3s;

FECHA=$(date +"%d-%m-%Y")

BACKUP=/root/backup/
DATABASE=/root/backup/database/
SITE=/root/backup/wordpress/

DOCUMENT_ROOT=/var/www/wordpress/

function dir_working () {

	mkdir -p ${DATABASE} && mkdir -p ${SITE}
}

function db_backup () {

	NAME_BCK_DB="DB_WORDPRESS_"${FECHA}.sql

	cd ${DOCUMENT_ROOT} || exit

	DB_NAME="$(grep DB_NAME wp-config.php | awk -F \' '{ print $4}')"
	DB_USER="$(grep DB_USER wp-config.php | awk -F \' '{ print $4}')"
	DB_PASSWORD="$(grep DB_PASSWORD wp-config.php | awk -F \' '{ print $4}')"

	mysqldump -u ${DB_USER} -p${DB_PASSWORD} ${DB_NAME} > ${DATABASE}${NAME_BCK_DB}
}

function db_site () {

	cp -R ${DOCUMENT_ROOT}* ${SITE}
	chown www-data:www-data ${SITE}
}

function db_tar () {

	cd ${BACKUP} || exit
	tar -zcf backup_${FECHA}.tar.gz ./* --remove-files

}

function all_checking () {

	echo " "
	echo "************************************************"
	
	echo " "
	echo "Contenido del directorio /root/backup/:"
	ls ${BACKUP}
	echo " "
	
	sleep 1s;
	
	echo "Tamaño del backup:"
	du -xhs ${BACKUP}*
	echo " "

	echo "************************************************"
	echo " "
}

# Funciones a ejecutar

dir_working
db_backup
db_site
db_tar
all_checking