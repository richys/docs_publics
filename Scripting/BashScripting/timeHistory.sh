#!/bin/bash

#
# Este script sirve para poner fecha y hora al comando history.
# Author: richys
# Fecha: mié sep 29 03:07:13 CEST 2021
#

FECHA_HORA=$(date +"%d/%b/%Y %H:%M")
RUTA="/etc/profile.d"

#Comprobación si tenemos permisos de administrador.
if [[ ${UID} -ne 0 ]]; then
  echo "Debes ejecutar el script con permisos de admistrador: sudo ${0}"
  exit 1
fi

#Creación de script + contenido.
cat <<EOF> ${RUTA}/timeHistory.sh
export HISTTIMEFORMAT="${FECHA_HORA} " 
EOF

exit 0
