#!/bin/bash

#
# Author: richys - Este script hace un ping a un Dominio o IP y te dice si esta online o no.
# Fecha: vie jun 19 14:42:28 CEST 2020
#

echo "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
cat << "EOF"

__     ___                      __  __                 _        
\ \   / (_)_   _____     ___   |  \/  |_   _  ___ _ __| |_ ___  
 \ \ / /| \ \ / / _ \   / _ \  | |\/| | | | |/ _ \ '__| __/ _ \ 
  \ V / | |\ V / (_) | | (_) | | |  | | |_| |  __/ |  | || (_) |
   \_/  |_| \_/ \___/   \___/  |_|  |_|\__,_|\___|_|   \__\___/ 
                                                                
                                                      By RICHYS

EOF
echo "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
echo " "

read -p "Dime el nombre dominio o de una IP, y te digo si esta Online: " DOMAIN

if [[ -z ${DOMAIN} ]]; then
	echo "NO me has dado un dominio, hablamos luego bye bye"
	exit 1;
fi

ping -c1 -W1 ${DOMAIN} &> /dev/null

if [[ "$?" == "0" ]]; then
	echo -e "[+] El dominio ${DOMAIN} \e[92mesta online.\e[49m\n"
else 
	echo -e "[-] El dominio ${DOMAIN} \e[91mno esta online.\e[49m\n"
fi

exit;