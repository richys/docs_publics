#!/bin/bash

# Este Script te ayuda con la instalación de el navegador Tor Browser,
# en 2 idiomas, Español y Ingles, en version de x64 bits en el SO Debian.
#
# La versión que instala es siempre la ultima estable, que se encuentre
# en el servidor de proyecto Tor.
#
# Un Saludo
# RichyS
#

# Programas esenciales para poder trabajar

if [[ $(dpkg -l | grep -wE "curl|html2text|wget" | wc -l) -ne 3 ]]; then
  echo -e "\e[93m\nHola! Antes de continuar con la instalación, debemos de instalar unos 
cuantos programas, que nos haran falta; estos son: curl, html2text, wget.\n\e[39m"
sudo apt update &> /dev/null && sudo apt -y install curl html2text wget &> /dev/null && clear
fi

VERSION=$(curl -s https://dist.torproject.org/torbrowser/ | html2text | awk '{print $2}' | tr "\/" " " | grep -Ev "Index|Parent_Directory|update|Server|]|a" | sed '/^$/d' | sort | tail -n 1 | tr -d '[[:space:]]')
PROGRAMA="Tor Browser"

echo -e "\e[92m
 _____            ____                                  
|_   _|__  _ __  | __ ) _ __ _____      _____  ___ _ __ 
  | |/ _ \| '__| |  _ \| '__/ _ \ \ /\ / / __|/ _ \ '__|
  | | (_) | |    | |_) | | | (_) \ V  V /\__ \  __/ |   
  |_|\___/|_|    |____/|_|  \___/ \_/\_/ |___/\___|_|
  
                                            By RICHYS  
                                                        
\e[39m"

echo -e "\nEste script te facilitara la instalación y configuración de ${PROGRAMA}\nen este caso la ${VERSION} que es la ultima estable :)\n"

read -p "Estas seguro que quieres instalar ${PROGRAMA} [ Y/N ]: " OPCION

if [[ "$OPCION" == "Y" ]] || [[ "$OPCION" == "y" ]]; then
  echo -e "\e[92m[+] Vamos a instalar ${PROGRAMA} :)\e[39m\n";
  sleep 1s;
else
  echo "Has decidido no instalar el programa :("
  sleep 1s;
  echo -e "Saliendo...\n"
  exit 1;
fi

function installEspañol (){
  echo "Empezamos: "
  mkdir ${HOME}/.tor; cd ${HOME}/.tor || exit 
  echo -e "\e[92m[+] Descargando el programa.\e[39m"
  wget -q https://dist.torproject.org/torbrowser/${VERSION}/tor-browser-linux64-${VERSION}_es-ES.tar.xz
  echo -e "\e[92m[+] Descomprimiendo el programa.\e[39m"
  tar xf tor-browser-linux64-${VERSION}_es-ES.tar.xz
  rm tor-browser-linux64-${VERSION}_es-ES.tar.xz
  cd tor-browser_es-ES || exit
  echo -e "\e[92m[+] Empieza la instalación.\e[39m"
  ${HOME}/.tor/tor-browser_es-ES/start-tor-browser.desktop --detach --register-app > /dev/null
  sleep 1s;
  echo -e "\nHemos terminado de instalar ${PROGRAMA} puedes revisar tu aplicaciones,\ny veras ya instalado ${PROGRAMA}.\n"
}

function installIngles (){
  echo "Empezamos: "
  mkdir ${HOME}/.tor; cd ${HOME}/.tor || exit
  echo -e "\e[92m[+] Descargando el programa.\e[39m"
  wget -q https://dist.torproject.org/torbrowser/${VERSION}/tor-browser-linux64-${VERSION}_en-US.tar.xz
  echo -e "\e[92m[+] Descomprimiendo el programa.\e[39m"
  tar xf tor-browser-linux64-${VERSION}_en-US.tar.xz
  rm tor-browser-linux64-${VERSION}_en-US.tar.xz
  cd tor-browser_en-US || exit
  echo -e "\e[92m[+] Empieza la instalación :)\e[39m"
  ${HOME}/.tor/tor-browser_en-US/start-tor-browser.desktop --detach --register-app > /dev/null
  sleep 1s;
  echo -e "\nHemos terminado de instalar ${PROGRAMA} puedes revisar tu aplicaciones,\ny veras ya instalado ${PROGRAMA}.\n"
}

read -p "Desea la instalación en Inglés (EN) o Español (ES): [ ES/EN ]: " IDIOMA

if [[ "$IDIOMA" == "ES" ]] || [[ "$IDIOMA" == "es" ]]; then
  echo -e "\e[92m[+] Vamos a instalar ${PROGRAMA} en Español (ES)\e[39m\n";
  sleep 1s;
  tput civis
  installEspañol;
elif [[ "$IDIOMA" == "EN" ]] || [[ "$IDIOMA" == "en" ]]; then
  echo -e "\e[92m[+] Vamos a instalar ${PROGRAMA} en Inglés (EN)\e[39m\n";
  sleep 1s;
  tput civis
  installIngles;
else
  echo -e "No me has dicho ninguna opción valida, se cierra el script :(\n"
  exit 1;
fi

sleep 1s;
echo -e "\e[92m[*] El Script de instalación de ${PROGRAMA} ha finalizado.\e[39m\n"
tput cnorm
