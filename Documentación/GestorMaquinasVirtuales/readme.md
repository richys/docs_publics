# Instalacion de KVM grafico en Debian 

## Pasos

**Logearnos como root**

`su -`

**Actualizar el sistema operativo.**

`apt update && apt upgrade -y`

**Instalar el paquete virt-manager.**

`apt install virt-manager -y`

**Dar permisos a nuestro usuario para que pueda virtualizar.**

> *USUARIO es el nombre de tu usuario no root.*

`adduser USUARIO libvirt` 

`adduser USUARIO libvirt-qemu`

#### date: Fri 17 Apr 2020 06:13:52 PM CEST

**Instalar el cliente [SPICE](https://www.spice-space.org/download.html)**

`apt install spice-client-gtk -y` 

**Reiniciamos nuestro PC**

`reboot`

> Después de haber reiniciado, buscamos en nuetro pc el programa "Gestor de Máquinas Virtuales", si tenemos experiencia con otros programas de virtualización, este es parecido pero mas pro, ya que es [KVM](https://es.wikipedia.org/wiki/Kernel-based_Virtual_Machine).

Continuará... 

# EOF