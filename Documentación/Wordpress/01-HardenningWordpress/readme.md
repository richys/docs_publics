# Fortificación de wordpress

### Definir los permisos correctamente.

`find /var/www/wordpress/ -type d -exec chmod 755 {} \;`

`find /var/www/wordpress/ -type f -exec chmod 644 {} \;`

### Eliminar fichero de instalación.

`rm -v /var/www/wordpress/readme.html && rm -v /var/www/wordpress/license.txt && rm -v /var/www/wordpress/wp-config-sample.php`

`rm -v /var/www/wordpress/wp-admin/install.php && rm -v /var/www/wordpress/wp-admin/install-helper.php`

### Permitir actualizaciones automaticas de wordpress.

> Pensarnoslo 2 veces si lo queremos, ya que muchas veces los plugins no compatible con nuestra nueva versión, "Recomiendo hacerlo desde el navegagador".

**Añadir esta linea en nuestro fichero `wp-config.php`**

`define( 'WP_AUTO_UPDATE_CORE', true );`

### Desahabiliar el editor de fichero online que trae wordpress.

**Añadir esta linea en nuestro fichero `wp-config.php`**

`define('DISALLOW_FILE_EDIT', true);`

### Protección del directorio wp-includes y del archivo wp-config.php.

**Añadir estas lineas en nuestro fichero `/etc/nginx/sites-available` de nuestro sitio**

```sh
location ~ ^/wp-admin/includes/ {
  return 403;
}

location ~ ^/wp-includes/[^/]+\.php$ {
  return 403;
}

location ~ ^/wp-includes/js/tinymce/langs/.+\.php {
  return 403;
}

location ~ ^/wp-includes/theme-compat/ {
  return 403;
}

# Proteccion del archivo wp-config.php.

location /wp-config.php {
  deny all;
}
```

### Cambiar el prefijo de las tablas de la base de datos

Esto lo recomiendo hacer cuando instalamos wordpress por primera vez en el formulario inicial, nuestro campo es `Table Prefix`.

**Usaremos el comando `pwgen` para gener el prefijo que usaeremos, acordarnos que debe de terminar en `_`.**

```sh
richys@debiana:~$ pwgen -nsa 7 1
T1Gxs8G
richys@debiana:~$

# Prefix Table

T1Gxs8G_
```

# EOF