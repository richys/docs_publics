# Migración de Wordpress Manualmente

## Paso 1

### Hacer backup entero de nuestro site, copiar la carpeta de wordpress entera.

`tar czvf siteWordpress.tar.gz wordpress/`

### Haccer copia de seguridad de nuestra BD del wordpress.

```sh
$ mysqldump -u root -p wordpress > data-dump.sql

$ head -n 5 data-dump.sql #comprobacion de nuestra BD
```

## Paso 2

### En el nuevo server

1. Descomprimir el wordpress y colocarlo en la ruta /var/www/

```sh
$ cp siteWordpress.tar.gz /var/www

$ cd /var/www/

$ tar xvf siteWordpress.tar.gz 

$ chown www-data: wordpress/ -R
```

2. Crear una BD con el nombre de nuestra base de datos enterior, y crear el usuario que estaba anteriormente y darle permisos.

```sh
$ mysql -u root

MariaDB [(none)]> create database wordpress;
MariaDB [(none)]> grant all privileges on wordpress.* to wpadmin@localhost identified by 'password';
MariaDB [(none)]> flush privileges;
```

3. Importación de la BD.

```sh
mysql -u wpadmin -p wordpress < data-dump.sql
```

> Poner la contraseña, del usuario y comprobamos si todo funciona :)

4. Apuntar el nginx al dominio, y rezar.

5. Si todo nos ha salido bien, deberemos ya de poder ver nuestra web.

# EOF