# Instalación de Wordpress con LEMP

### Hacernos super-usuarios.

`sudo su -`

### Instalación de paquetes que utilizaremos y que seran necesarios.

`apt install wget curl vim tmux net-tools nginx dnsutils bash-completion mariadb-server php-fpm php-mysql php-gd php-xml php-mbstring php-xmlrpc php-intl php-zip php-curl php-imagick -y`

### Crear direcorios de nuestra descarga.

`mkdir ~/downloads; cd ~/downloads`


### Descargar el proyecto de wordpress.

> En esta caso es una versión en español y la ultima versión estable, si queremos otra versión y con otro idioma visitar la web [Wordpress](https://wordpress.org/download/).

`curl https://es.wordpress.org/latest-es_ES.tar.gz | tar xvz`

`cp -rv wordpress/ /var/www/ && chown www-data:www-data /var/www/wordpress -R`


### Creación de la BD

**Securizar el acceso a la DB.**

`mysql_secure_installation`

**Entrar en la DB.**

`mysql -u root -p`

**Creación de la DB.**

`create database wordpress;`

**Creación de usuario que controlara la DB de wordpress.**

`grant all privileges on wordpress.* to usuario@localhost identified by 'PASSWORD';`

**Hacer posibles los cambios.**

`flush privileges;`

**Salir del cliente de DB.**

`exit`

### Fichero nginx configuración.

**Configuración basica del fichero default de nginx**

`vi /etc/nginx/sites-available/default`

**Cambiar las siguientes lineas**

`root /var/www/html;` -->  `root /var/www/wordpress/;`

**Añadir index.php**

`index index.html index.htm index.nginx-debian.html;` --> `index index.php;`

**Definir dominio, si tenemos.**

`server_name _;` --> `server_name dominio.com;` 

**Desmarcar las lineas de pass PHP; guardar y salir**

```sh
# Original comentado

#location ~ \.php$ {
        #       include snippets/fastcgi-php.conf;
        #
        #       # With php-fpm (or other unix sockets):
        #       fastcgi_pass unix:/run/php/php7.3-fpm.sock;
        #       # With php-cgi (or other tcp sockets):
        #       fastcgi_pass 127.0.0.1:9000;
        #}

# Descomentado

location ~ \.php$ {
               include snippets/fastcgi-php.conf;
               fastcgi_pass unix:/run/php/php7.3-fpm.sock;
        }
```

### Reinciar los servicios de PHP y NGINX

`for i in nginx.service php7.3-fpm.service; do systemctl stop $i; sleep 2; systemctl restart $i; echo "OK $i"; done`

# EOF