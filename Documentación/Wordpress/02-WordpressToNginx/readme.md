# Ejemplo de fichero de NGINX para Wordpress.

### Fichero ya ok para producción.

Este fichero ya cuenta con los certificados activos, hay un tutorial sobre esto aplicarlo antes de nada [Tutorial Certs](https://gitlab.com/richys/docs_publics/-/tree/master/Documentaci%C3%B3n/SSL#obtener-verificaci%C3%B3n-a-en-ssl-con-lets-encrypt)

```sh
server {
    listen 80;
    server_name DOMAIN.com;
    return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl http2;
    server_name DOMAIN.com;

    ssl_certificate /etc/letsencrypt/live/DOMAIN.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN.com/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/DOMAIN.com/chain.pem;

    root /var/www/wordpress/;

    index index.php index.html;
    
    error_page 404 /index.php;

    location / {
        try_files $uri $uri/ /index.php;
    }
    
    location = /favicon.ico {
                log_not_found off;
                access_log off;
        }

        location = /robots.txt {
                allow all;
                log_not_found off;
                access_log off;
        }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
                expires max;
                log_not_found off;
        }

    location ~ \.php$ {
               include snippets/fastcgi-php.conf;
               fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
        }
    location ~ ^/wp-admin/includes/ {
                return 403;
        }

    location ~ ^/wp-includes/[^/]+\.php$ {
                return 403;
    }
    
    location ~ ^/wp-includes/js/tinymce/langs/.+\.php {
                return 403;
    }
    
    location ~ ^/wp-includes/theme-compat/ {
                return 403;
    }
    
    # Proteccion del archivo wp-config.php.
    
    location /wp-config.php {
                deny all;
    }

}
```