# Configurar PHP y NGINX para subir ficheros del tamaño de 100MB.

### Comprobamos las lineas que tenemos actualmente configuradas.

> En este caso estamos usando php7.3, revisar cual es el tuyo.

`cat /etc/php/7.3/fpm/php.ini | grep 'memory_limit\|post_max_size\|upload_max_filesize\|file_uploads\|max_execution_time\|max_input_time'`

### Configuración de ejemplo que debemos de cambiar.

`vi /etc/php/7.3/fpm/php.ini`

```text
max_execution_time = 360
max_input_time = 60
memory_limit = 256M
post_max_size = 100M
file_uploads = On
upload_max_filesize = 100M
max_file_uploads = 20
```

### Debemos de editar nuestro fichero de configuración web del nginx, que se encuentra en el directorio /etc/nginx/sites-availables/.

`vi /etc/nginx/sites-available/default`

**Añadir esta linea**

> Debe de llevar el mismo valor hemos puesto en `upload_max_filesize = 100M`

`client_max_body_size 100M;`                                                                                                      

### Reinciar el servicio de NGINX

`systemctl restart nginx.service` 

# EOF