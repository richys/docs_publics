# Distinstas formas de comprobar si nuestro archivo es legal

## MD5

- Vamos a comprobar le integridad de la iso del Sitema Operativo Debian
```sh
richys@debiana:~/pruebas$ ls -l
total 343048
-rw-r--r-- 1 richys richys 351272960 Feb  8 14:47 debian-10.3.0-amd64-netinst.iso
-rw-r--r-- 1 richys richys       274 Feb  9 02:58 MD5SUMS
richys@debiana:~/pruebas$
```

- Vemos cual es el hashs de nuestra iso

```sh
richys@debiana:~/pruebas$ md5sum -b debian-10.3.0-amd64-netinst.iso 
5956434be4b81e6376151b64ef9b1596 *debian-10.3.0-amd64-netinst.iso
richys@debiana:~/pruebas$
```

- Compobar la integridad con el fichero que tenemos.

```sh
richys@debiana:~/pruebas$ md5sum -c --ignore-missing MD5SUMS 
debian-10.3.0-amd64-netinst.iso: OK
richys@debiana:~/pruebas$
```
> El OK quiere decir que nuestra ISO concuerda con el fichero de MD5.


## SHA256

- Vamos a comprobar le integridad de la iso del Sitema Operativo Debian

```sh
richys@debiana:~/pruebas$ ls -l
total 343048
-rw-r--r-- 1 richys richys 351272960 Feb  8 14:47 debian-10.3.0-amd64-netinst.iso
-rw-r--r-- 1 richys richys       402 Feb  9 02:58 SHA256SUMS
richys@debiana:~/pruebas$ 
```

- Vemos cual es el hashs de nuestra iso

```sh
richys@debiana:~/pruebas$ sha256sum -b debian-10.3.0-amd64-netinst.iso 
6a901b5abe43d88b39d627e1339d15507cc38f980036b928f835e0f0e957d3d8 *debian-10.3.0-amd64-netinst.iso
richys@debiana:~/pruebas$ 
```

- Compobar la integridad con el fichero que tenemos.

```sh
richys@debiana:~/pruebas$ sha256sum -c --ignore-missing SHA256SUMS 
debian-10.3.0-amd64-netinst.iso: OK
richys@debiana:~/pruebas$
``` 

> El OK quiere decir que nuestra ISO concuerda con el fichero de SHA256.


# SHA512

- Vamos a comprobar le integridad de la iso del Sitema Operativo Debian.

```sh 
richys@debiana:~/pruebas$ ls -l
total 343048
-rw-r--r-- 1 richys richys 351272960 Feb  8 14:47 debian-10.3.0-amd64-netinst.iso
-rw-r--r-- 1 richys richys       658 Feb  9 02:58 SHA512SUMS
richys@debiana:~/pruebas$
```

- Vemos cual es el hashs de nuestra iso

```sh
richys@debiana:~/pruebas$ sha512sum -b debian-10.3.0-amd64-netinst.iso 
08962831a26cad19ac5e1418a5f907a907d375c6d51be608281f5b733c248d7bd4008439af224f3d52df2e500f38e939e1bd46dd9371b2bdc7101b0efcb65634 *debian-10.3.0-amd64-netinst.iso
richys@debiana:~/pruebas$
```

- Compobar la integridad con el fichero que tenemos.

```sh
richys@debiana:~/pruebas$ sha512sum -c --ignore-missing SHA512SUMS 
debian-10.3.0-amd64-netinst.iso: OK
richys@debiana:~/pruebas$ 
```

> El OK quiere decir que nuestra ISO concuerda con el fichero de SHA512.

# Actualizar más formas de checking.