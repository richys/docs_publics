# Actualizar BIOS en PC thinkpad

## Entrar en la web y buscar el modelo del PC.

[Ir a la web](https://pcsupport.lenovo.com/es/es/)

Descargar version: **BIOS Update (Bootable CD)**

## Instalar tool para convertir imagen en IMG.

```sh
apt update && apt install genisoimage -y
```

## Convertir imagen ISO a IMG.

```sh
# geteltorito ‐o {output‐image‐name.img} {Bootable‐CD.iso}
geteltorito ‐o BIOS.img CODE.iso
```

## Crear booteable.

```sh
dd if=BIOS.img of=/dev/sdb status=progress
```
# EOF
