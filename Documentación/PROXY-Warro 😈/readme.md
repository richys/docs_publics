### Pasos para la instalacion de nuestro servidor Proxy Warro.

### Actualizar nuestro SO

`apt update && apt upgrade -y`

### Instalación del proxy y servidor web

```sh
apt install squid3 apache2-utils -y
```

### Hacemos backup de nuestro fichero original de SQUID

```sh
# No movemos al directorio
cd /etc/squid

# Creamos el Backup
cp squid.conf squid.conf.bck
```

## Crear de nuestro proxy 

### Editamos fichero de SQUID

```sh
vi squid.conf
```

### Buscamos la linea 

`http_access allow localhost` y la editamos por `http_access allow all`

### Añadimos estas lineas al fichero.

```sh
#IP_SERVER: IP de nuestro servidor VPS ;)

acl ip1 myip IP_SERVER
tcp_outgoing_address IP_SERVER ip1

forwarded_for off
request_header_access Allow allow all
request_header_access Authorization allow all
request_header_access WWW-Authenticate allow all
request_header_access Proxy-Authorization allow all
request_header_access Proxy-Authenticate allow all
request_header_access Cache-Control allow all
request_header_access Content-Encoding allow all
request_header_access Content-Length allow all
request_header_access Content-Type allow all
request_header_access Date allow all
request_header_access Expires allow all
request_header_access Host allow all
request_header_access If-Modified-Since allow all
request_header_access Last-Modified allow all
request_header_access Location allow all
request_header_access Pragma allow all
request_header_access Accept allow all
request_header_access Accept-Charset allow all
request_header_access Accept-Encoding allow all
request_header_access Accept-Language allow all
request_header_access Content-Language allow all
request_header_access Mime-Version allow all
request_header_access Retry-After allow all
request_header_access Title allow all
request_header_access Connection allow all
request_header_access Proxy-Connection allow all
request_header_access User-Agent allow all
request_header_access Cookie allow all
request_header_access All deny all
```

### Reiniciar el servidor de SQUID

```sh
service squid restart 
```

### Modo de conexión

Nuestra configuración es para un servidor proxy web, si lo quedamos consumir debemos ir a:

1. Opciones de nuestro navegador "Mozilla".
2. Configuración de red.
3. Proxy HTTP `IP_SERVER` Puerto `3128`
4. [X] Usar el mismo proxy para todo.
5. Listo todo configurado 🥰.

Un Saludo 😎

<EOF>