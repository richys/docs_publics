# Obtener verificación A+ en ssl con Let's Encrypt

En este tutorial se obtendra calificación A+ en **SSL Labs**, usaremos el SO Debian 10 y Nginx

## Instalar los paquetes.

`apt install nginx certbot letsencrypt -y`

## Generar el cerficiado el certificado SSL dhparam.

`openssl dhparam -out /etc/nginx/dhparam.pem 4096`

> El proceso puede tardar un tiempo, todo dependera de los recursos de nuestra VM, 10 Min aprox.

## Añadir configuración en el servidor NGINX.

Añadir esto en el fichero /etc/nginx/nginx.conf; debajo de SSL Settings

```txt
        ssl_protocols TLSv1.3;# Requires nginx >= 1.13.0 else use TLSv1.2
        ssl_prefer_server_ciphers on;

        ssl_dhparam /etc/nginx/dhparam.pem;
        
        ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';

        ssl_ecdh_curve secp384r1;

        ssl_session_timeout 10m;
        ssl_session_cache shared:SSL:10m;
        ssl_session_tickets off;

        ssl_stapling on;
        ssl_stapling_verify on;
        resolver 8.8.8.8 8.8.4.4 valid=300s;
        resolver_timeout 5s;
        
        add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
        add_header X-Frame-Options DENY;
        add_header X-XSS-Protection "1; mode=block";
        add_header X-Content-Type-Options nosniff;
        add_header X-Robots-Tag none;
        
        gzip_min_length 256;
        gzip_disable "msie6";
        
        gzip_vary on;
        gzip_proxied any;
        gzip_comp_level 6;
        gzip_buffers 16 8k;
        gzip_http_version 1.0;
        gzip_types text/plain text/css application/json application/x-javascript application/javascript text/xml application/xml application/xml+rss text/javascript text/x-js;
```

## Ahora lo que haremos sera generar nuestros Certificados.

`systemctl stop nginx.service`

`certbot certonly --standalone --rsa-key-size 4096 --agree-tos --no-eff-email --email email@email.com -d domain.com`

> Probar sustituir --standalone por nuestro servidor --nginx --apache.

## Ejemplo de fichero de configuración.

```txt
server {
    listen 80;
    server_name DOMAIN.com;
    return 301 https://$server_name$request_uri;
}

server {
    listen 443 ssl http2;
    server_name DOMAIN.com;

    ssl_certificate /etc/letsencrypt/live/DOMAIN/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/DOMAIN/chain.pem;

    root /var/www/html/;

    index index.php index.html;

    location / {
               try_files $uri $uri/ =404;
       }
}
```

## Antes de reinciar el servidor, comprobaremos que todo lo que tenemos esta bien

`nginx -t`

`systemctl restart nginx.service` 


## Crear tarea de cron para renovar los certs automaticamente, ya que estos caducan a los 3 meses.

`crontab -e`

**Añadir estas lineas**

`0 0 1 */2 *  /usr/bin/certbot renew --pre-hook "service nginx stop" --post-hook "service nginx start"`

**Reiniciar servicio de CRON**

`systemctl restart cron.service`

## Test de nuestros configuración

`echo QUIT | openssl s_client -connect DOMAIN:COM:443 -status 2> /dev/null | grep -A 17 'OCSP response:' | grep -B 17 'Next Update'`

`curl -s -D- https://DOMAIN.com | grep -i Strict`

## Visitar la web 

[Checking SSL](https://www.ssllabs.com/ssltest/index.html) Poner nuestro dominio y comprobar el A+.

# EOF
