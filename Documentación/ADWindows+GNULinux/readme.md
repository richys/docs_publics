# Configuración PBIS en Ubuntu 18.04 LTS - Kernel 4.15.0-23-generic

## Resumen 

Esto es un pequeño tutorial sobre PBIS, esta aplicacion sirve para ayudarnos a unir aun equipo de GNU/Linux a un Directorio Activo de Windows.

## Actualizar el Sistema

`apt update && apt upgrade -y`

## Editar el fichero de orden de búsquedas de red

`vi /etc/nsswitch.conf`

Cambiamos el orden del parámetro dns y que así pueda resolver DNS
con la info que le llega por DHCP del controlador de dominio.
Asi debe de quedar:

```
hosts: files dns m dns4_minimal...
``` 

## Instalacion y configuracion SSH

Instalar ssh:

`apt install ssh -y`

Fichero de configuración de ssh:

`vi /etc/ssh/sshd_config`

Comprobar las siguientes configuraciones del fichero ssh:

```sh
Use PAM yes
ChallengeResponseAuthentication yes
```

Fortificación del fichero:

`chattr +i /etc/ssh/sshd_config`

## Configuración fichero DHCP

Editar el fichero:

`vi /etc/dhcp/dhclient.conf`

Descomentar y editar las líneas:

```sh
supersede domain-name "confianza.tic";
prepend domain-name-servers 192.168.2.250;
```

Fortificación del fichero:

`chattr +i /etc/ssh/sshd_config`

## Instalación de PBIS

#### Visitar la web del proyecto para comprobar la versión de PBIS

https://github.com/BeyondTrust/pbis-open/releases

Descargar la versión de PBIS (v.8..0):

`wget https://github.com/BeyondTrust/pbis-open/releases/download/8.8.0/pbis-open-8.8.0.506.linux.x86_64.deb.sh`

Dar permisos al script:

`chmod +x pbis*`

Ejecutar PBIS:

`./pbis*`

Ejecutamos lo siguiente para Definir Shell y Home de usuarios:

```sh
/opt/pbis/bin/config HomeDirTemplate %H/%D/%U
/opt/pbis/bin/config LoginShellTemplate /bin/bash
```

Unir Equipo al Dominio:

`domainjoin-cli join confianza.tic administrador`

**En caso de error SSH, ejecutar los comandos de abajo**

`domainjoin-cli join --disable ssh confianza.tic administrador`

## Definir configuraciones de autenticación

`vi /etc/pam.d/common-session`

Modificar línea:

`session optional pam_lsass.so`

Deberá de quedar así:

`session [success=ok default=ignore] pam_lsass.so`

## Editar Fichero de Sudoers

`vi /etc/sudoers`

Añadimos:

`%CONFIANZA\\empleados ALL=(ALL) ALL`

Instalación de krb5-user

`apt install krb5-user -y`

## Editar GDM3

**Ocultar Logins de Usuarios, preferibles en caso donde se comparte Ordenador**

Editamos el fichero de configuración de inicio de sesión de GDM3, para que nos nos muestre los demás usuarios entes del login.

`vi /etc/gdm3/greeter.dconf.defaults`

Descomentar las líneas:

```sh
[org/gnome/login-screen]
disable-user-list=true
```

Fortificación del fichero:

`chattr +i /etc/gmd3/greeter.dconf.defaults`

Reiniciar Sistema

`reboot`

# EOF