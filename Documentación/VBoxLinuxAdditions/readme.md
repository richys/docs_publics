# Problema con las Guest Addition

Hola a todxs,

Este problema o bug, me lo he encontrado a la hora de querer instalar las Guest Addition en un Maquina virtual de linux, y despues de recorrer por internet he dado con la solución, asi que decide documentarla para evitar futuras busquedas.

Nota: como maquina anfitriona tengo un Debian 9, si usas maquina anfitriona Windows; no hay problema.

## Pasos

1. Actualizamos los repositorios y el sistema operativo.

` apt update && apt upgrade -y`

2. Instalar unos paquetes esenciales.

`apt install build-essential module-assistant -y`

3. Ahora agregamos nuestros Modules Assistant.

`m-a prepare -y`

4. Montamos nuestro CD-ROM con las Guest Addition.

`mount /dev/sr0 /media`

> Buscamos el script VBoxLinuxAdditions.run y lo copiamos a nuestro /home/

`cp /media/VBoxLinuxAdditions.run $HOME`

5. Ejecutamos y instalamos el script.

`bash VBoxLinuxAdditions.run`
    
Si nos da error no pasa nada, no nos asustemos :).

6. Reiniciamos el Sistema Operativo.

`reboot`

7. Iniciamos nuestra Maquina virtual y comprobamos la instalacion.

## Hacerlo en un solo paso.

```sh
apt update && apt upgrade -y; apt install build-essential module-assistant -y; m-a prepare -y
```

8. EOF
