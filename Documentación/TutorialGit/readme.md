## Tutorial de Git

## Instalación de Git en sistema GNU/Linux Debian

`apt update && apt install git -y`

## Crea un directorios en el que vamos a trabajar 

`mkdir pruebasGit`

`cd pruebasGit && touch readme.md`

## Configuración Inicial

Definir nombre de usuario que hara los commits

`git config --global user.name USUARIO`

Definir el correo del usuario que hara los commits

`git config --global user.email CORREO@CORREO.COM`

Definir colores de errores y demas

`git config --global color.ui true`

Ver toda nuestra configuración 

`git config --global --list`

Deberias de ver algo asi 

```txt
user.name=richys
user.email=richys@correo.com
color.ui=true
```

## Vamos a trabajar con git

Con el siguiente comando iniciaremos el git para que empiece a trabajar

`git init`

El siguiente comando nos permite saber en todo momento que cambios tenemos en nuestro proyecto

`git status`

Para actualizar nuestros cambios del fichero `readme.md` debemos de usar el siguiente comando

`git add NOMBRE`

si volvemos a usar el comando `git status` veremos que ya esta añadido

Ahora guardaremos los cambios que acabamos de hacer, para esto usaremos el comando

`git commit -m "MENSAJE"`

`git commit -m "Este es mi primer commit :)"`

> Con la opcion `git commit -am "MENSAJE"` creamos el commit y a la vez añadimos los cambios, es como 2 comandos en 1 solo `git commit y git add` 

Podemos ver los logs de todos nuestros commits que hemos ido haciendo

`git log`

Gracia a la tecnologia de los commits, podemos volver a un estado anterior de algun cambio

Ejecutamos el comando `git log` y nos fijamos en el hash del commit que queremos volver

```sh
git log 
commit 3c4000f5c0767083bd0b4e7ffab9b1fe5ab3cba3
Author: richys <richys@correo.com>
Date:   Fri Sep 27 15:16:52 2019 +0200

    Mi primer hello_World en el readme.md

commit 366ce372a74af2b6a6185d942df637a391e7b29d
Author: richys <richys@correo.com>
Date:   Fri Sep 27 15:10:51 2019 +0200

    Esta añadido el readme.md
```

Ahora con el comando `git checkout 366ce372a74af2b6a6185d942df637a391e7b29d` y lo que pasara es que "volveremos en el tiempo", de esta forma podremos detectar errore.

Ahora si queremos volver a donde estabamos, lo haremos con el siguiente comando

`git checkout master`

## Jugando con ramas "Lineas de tiempo"

Para ver las ramas que tenemos usamos el comando, la rama usada tendra un * 

`git branch`

Crear ramas 

`git branch NOMBRE` 

Para cambiarnos de ramas

`git checkout RAMA`

Borrar ramas que no queremos usar

`git branch -D RAMA`

### Fusiones "Juntar Ramas"

Se basa en la creacion de un nuevo commit que juntara las ramas.

*Pasos*

1. Situanos en la rama que vamos a juntar "La principal"

	`git checkout master`

2. Fusionar "Unir la ramas"
	`git merge RAMA`

3. Ahora si comprobamos todo, ya tenemos los nuevos cambios

## Clonar un proyecto existen en GitHub o GitLab o cualquier plataforma

Para clonar el repositorio, necesitamos la url de este mismo mas el siguiente comando

`git clone URL`

Añadir el repositorio "Conectar"

`git remote add origin https://gitlab.com/richys/prueba.git`

Comprobar los repositorios

`git remote -v`

Borrarlos

`git remote remove origin`

Enviar lo que tenemos en local a remoto

`git push origin RAMA`

`git push origin master`

## ISUES

Sirven para reportar errores en el proyecto
