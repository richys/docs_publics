# Instalacion de python desde paquete pre-compilado.

## Actualizamos el SO.

`apt update && apt upgrade`

## Instalamos paquetes necesarios.

`apt install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev`

## Descargar paquetes de python y descomprimir.

```sh
wget https://www.python.org/ftp/python/3.10.8/Python-3.10.8.tgz
tar xvf Python-3.10.8.tgz
```

## Compilar y instalar.

```sh
cd Python-3.10.8
./configure --enable-optimizations
make -j $(nproc)
make altinstall
```

## Definir por defecto la nueva version de python que hemos instalado.

```sh
# Buscar el path de la nueva version de python.
whereis python3.10.8

update-alternatives --install /usr/bin/python python /usr/local/bin/python3.10 1
```

## Eliminar la version instalada.

```sh
prefix='/usr/local/'
pyver='3.10'

rm -rf \
    $HOME/.local/lib/Python${pyver} \
    ${prefix}bin/python${pyver} \
    ${prefix}bin/python${pyver}-config \
    ${prefix}bin/pip${pyver} \
    ${prefix}bin/pydoc \
    ${prefix}bin/include/python${pyver} \
    ${prefix}lib/libpython${pyver}.a \
    ${prefix}lib/python${pyver} \
    ${prefix}lib/pkgconfig/python-${pyver}.pc \
    ${prefix}lib/libpython${pyver}m.a \
    ${prefix}bin/python${pyver}m \
    ${prefix}bin/2to3-${pyver} \
    ${prefix}bin/python${pyver}m-config \
    ${prefix}bin/idle${pyver} \
    ${prefix}bin/pydoc${pyver} \
    ${prefix}bin/pyvenv-${pyver} \
    ${prefix}share/man/man1/python${pyver}.1 \
    ${prefix}include/python${pyver}m
rm -rI ${prefix}bin/pydoc
```

# EOF
