# Hardening SSH 

### Rutas de configuración del fichero del Servidor de SSH. 

`/etc/ssh/sshd_config`

### Hacemos backup del fichero original, por si tengamos problemas podamos restablecerlo.

`cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bck`

### Comprobar las configuraciones que tenemos actualmente.

`ssh -T`

Este comando nos motrara todo la configuración que tenemos habilitada en nuestro servidor.

### Reiniciar, Comprobar estado del servicio.

`service ssh restart && service ssh status`

# Comenzamo la configuración de nuestro Servidor SSH.

`vi /etc/ssh/sshd_config`

1. Comprobar si tenemos activo el protocolo 2 de ssh, si no es asi lo activamos, escriendo la siguiente linea:
    
    `Protocol 2`

2. Desabilitar el acceso al usuario ROOT:

    `PermitRootLogin no`
    `UsePAM no` 
    
3. Controlar el tiempo de conexión hacia el server:
    
    ```
    ClientAliveInterval 300 #Tiempo en Segudos "5min".
    ClientAliveCountMax 2 #Acciones de comprobación antes de desconectarse.
    ```
4. Definir Lista Blanca [Usuarios que pueden iniciar sesión]:

    `AllowUsers richys user2 user3`
    
5. Cambiar de puerto nuestro servicio, ssh escucha por el puerto 22:

    `Port 12345`

6. Permitir solo conexiones SSH-Key [ Debemos de tener creada nuestra SSH-Key y subida al servidor]

    Generamos la Key - **" Definirle contraseña a la Key Siempre "**
    
    `ssh-keygen -b 4096 -t rsa `
    
    Copiamos la Key al Server
    
    ` ssh-copy-id richys@IPServer`
    
    #### Admin version Copy Key
    
    1. Crear usuario e ir a su /home/, y crear directorio Oculto de ssh:
    
        `mkdir .ssh`
    
    2. Definir permisos a dicho directorio:

        `chmod 700 .ssh`
        
    3. Entrar en el directorio y crear el siguiente fichero, y darle permisos:
    
        ```sh
        cd .ssh
        
        touch authorized_keys
        
        chmod 600 authorized_keys
        ```
    
    4. Copiar SSH-Key del usuario dentro del fichero [authorized_keys]:
    
        `vi authorized_keys [Pegar Key]`

7. Desabilitar uso de contraseña en login de SSH.

    `PasswordAuthentication no` 

8. Deshabilitamos inicio con contraseñas vacias:

    `PermitEmptyPasswords no`
    
9. Deshabiliar el usuario grafico de aplicaciones del Servidor.

    `X11Forwarding no` 

10. Buscar y descargar proyecto `ssh-audit.py`

    Este programa nos dira algunas paremetros que debemos de configurar, los seguiremos.

## EOF