# Baneando intentos fallidos de SSH.

### Ver cuantos intentos de acceso hemos tenido.

`lastb | wc -l`

### Instalación de fail2ban.

`apt update && apt install fail2ban -y && systemctl stop fail2ban.service`

### Configuración de fail2ban.

> Esta configuración es muy agresiva, ya que se banea la ip por 1 año.

`cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local`

`sed -i -e 's/^maxretry = 5/maxretry = 1/' /etc/fail2ban/jail.local`

`sed -i -e 's/^bantime  = 10m/bantime  = 1y/' /etc/fail2ban/jail.local`

### Parada y reincio del servicio fail2ban.

`for i in fail2ban.service; do systemctl stop $i; echo "Servicio $i DOWN"; sleep 2s; systemctl restart $i; echo "Servicio $i UP"; done`

# EOF