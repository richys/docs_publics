# Creación de subdominios onions con nginx.

Este es un pequeño tutorial de como crear crear Subdominios en url .onion

**By RichyS**

## Instalar tor y nginx

`apt install tor nginx -y`

## Crear Hidden Service HTTP

`vi /etc/tor/torrc`

Creamos nuestro Hidden Service.

```txt
.
.
.
############### This section is just for location-hidden services ###

## Once you have configured a hidden service, you can look at the
## contents of the file ".../hidden_service/hostname" for the address
## to tell people.
##
## HiddenServicePort x y:z says to redirect requests on port x to the
## address y:z.

HiddenServiceDir /var/lib/tor/hidden_service/
HiddenServicePort 80 127.0.0.1:80
.
.
.
```

### En este fichero se encuentra tu url .onion

`cat /var/lib/tor/hidden_service/hostname`

## Configuración de Virtual Host de nginx

`vi /etc/nginx/sites-available/torsite`

```txt
server {
	listen 127.0.0.1:80;

	root /var/www/onion/public/;

	index index.html;

    # Poner aqui nuestra URL
	server_name zwt6fvlw7vojks3utv7fzotk2slmm372zjpu3ihjjbewkiswewqyu6yd.onion;

    # Añadir el subdominio y la url de onion
	rewrite ^/(.*) http://www.zwt6fvlw7vojks3utv7fzotk2slmm372zjpu3ihjjbewkiswewqyu6yd.onion/$1 permanent;

	location / {
		try_files $uri $uri/ =404;
		autoindex off;
	}
}
server {
	listen 127.0.0.1:80;

	root /var/www/onion/public/;

	index index.html;

    # Añadimos nuestra url completa con el subdominio incluido.
	server_name www.zwt6fvlw7vojks3utv7fzotk2slmm372zjpu3ihjjbewkiswewqyu6yd.onion;

	location / {
		try_files $uri $uri/ =404;
		autoindex off;
	}
}
```

## Comprobamos la configuración de nginx.

`nginx -t`

Si nos da este error, debemos de añadir más tamaño:

```
nginx: [emerg] could not build server_names_hash, you should increase server_names_hash_bucket_size: 64
nginx: configuration file /etc/nginx/nginx.conf test failed
```

## Modificamos el valor en el fichero de configuración global de nginx.

`vi /etc/nginx/nginx.conf`

Buscar en el fichero la linea **server_names_hash_bucket_size: 64** y modificarla por **server_names_hash_bucket_size: 128**

## Comprobamos la configuración de nginx, para ver si ahora esta todo correcto.

`nginx -t`

## Reiniciamos el servicio de nginx

`systemctl restart nginx.service`

## Comprobamos que todo esta OK, ponemos nuestra URL en Tor Browser y listo :)

EOF
