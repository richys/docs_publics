# Tutorial montar particiones Cifradas automaticamente

## 1 - Crear fichero donde se encontrar nuestra KEY

`mkdir -p /etc/luks_keys`

`dd if=/dev/urandom of=/etc/luks_keys/key bs=512 count=8`

## 2 - Crear particion 

`cryptsetup -v luksFormat /dev/sdb1`

## 3 - Crear clave o añadir clave

`cryptsetup -v luksAddKey /dev/sdbX /etc/luks_keys/key`

## 4 - Comprobar si el fichero de clave funciona

`cryptsetup -v luksOpen /dev/sdb1 sdb1_crypt --key-file=/etc/luks_keys/key`

## 5 - Cerramos particion montada

`cryptsetup -v luksClose sdb1_crypt`

# Definir particion para que se monte en el arranque

## 1 - Buscar el UUID del disco

`blkid`

> ahora buscamos nuestra particion, sera algo de esta manera:

/dev/sdb1: UUID="1544c24e-00e3-455e-9194-40657d1de154" TYPE="crypto_LUKS" PARTUUID="05c489bd-01"

> tenemos que copiar el UUID los numeros sin las comillas

## 2 - Vamos a la ruta y definimo hay nuestro dispositivo

`vi /etc/crypttab`

> añadimos el final de la linea

sdb1_crypt UUID=1544c24e-00e3-455e-9194-40657d1de154 /etc/luks_keys/key luks

## 3 - Validar nuestra particion

`cryptdisks_start sdb1_crypt`

## 4 - Habilitar el montaje de Inicio

`vi /etc/fstab`

> añadir al final del fichero

/dev/mapper/sdb1_crypt /mnt/VirtualMachines ext4    defaults   0       2

## 5 - Reiniciamos el PC y ya tendremos todo OK

`reboot` 

# EOF



